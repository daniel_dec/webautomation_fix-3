﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.WebElementProviders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Base class for Web Element providers.
    /// </summary>
    public abstract class WebElementProvider : IWebElementProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebElementProvider" /> class.
        /// </summary>
        public WebElementProvider()
        {
            this.Name = string.Empty;
            this.ResetWebDriverToDefaultContent = true;
            this.SearchExpression = string.Empty;
            this.SearchParameters = new string[0];
        }

        /// <summary>
        /// Gets or sets the name of Web Component.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Web Driver.
        /// </summary>
        public IWebDriver WebDriver { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the WebDriver should be reset to default content before searching the element.
        /// </summary>
        public bool ResetWebDriverToDefaultContent { get; set; }

        /// <summary>
        /// Gets the Web Element.
        /// </summary>
        public IWebElement WebElement
        {
            get
            {
                if (this.ResetWebDriverToDefaultContent)
                {
                    this.WebDriver.SwitchTo().DefaultContent();
                }

                try
                {
                    return this.WebDriver.FindElement(this.SearchBy);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the list of Web Elements.
        /// </summary>
        public IList<IWebElement> WebElements
        {
            get
            {
                if (this.ResetWebDriverToDefaultContent)
                {
                    this.WebDriver.SwitchTo().DefaultContent();
                }

                return this.WebDriver.FindElements(this.SearchBy);
            }
        }

        /// <summary>
        /// Gets or sets the search expression.
        /// </summary>
        public string SearchExpression { get; set; }

        /// <summary>
        /// Gets or sets the additional search parameters.
        /// </summary>
        public string[] SearchParameters { get; set; }

        /// <summary>
        /// Gets the full search expression (based on Search Expression and Search Parameters).
        /// </summary>
        protected virtual string FullSearchExpression
        {
            get
            {
                return this.SearchExpression;
            }
        }

        /// <summary>
        /// Gets the function that returns By object which is used for searching the Web Element.
        /// </summary> 
        protected virtual By SearchBy
        {
            get
            {
                return By.Id(this.FullSearchExpression);
            }
        }

        /// <summary>
        /// Gets the search method.
        /// </summary>
        protected virtual string SearchMethod
        {
            get
            {
                return "id";
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>String representation of the current object.</returns>
        public override string ToString()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("Name", this.Name);
            parameters.Add("Search by", this.SearchMethod);
            
            if (this.SearchParameters != null && this.SearchParameters.Length > 0)
            {
                string searchParameters = this.SearchParameters
                                        .Select(p => "'" + p + "'")
                                        .Aggregate<string>((p1, p2) => p1 + ", " + p2);

                parameters.Add("Parameters", searchParameters);
            }

            parameters.Add("Expression", this.FullSearchExpression);

            return LogFormatter.GetLog(parameters);
        }

        /// <summary>
        /// Clone current Web Element provider.
        /// </summary>
        /// <returns>New copy of current Web Element provider.</returns>
        public virtual IWebElementProvider Clone()
        {
            IWebElementProvider webElementProvider = (IWebElementProvider)Activator.CreateInstance(this.GetType());
            webElementProvider.SearchExpression = this.SearchExpression;
            webElementProvider.SearchParameters = this.SearchParameters;
            webElementProvider.Name = this.Name;
            webElementProvider.WebDriver = this.WebDriver;
            webElementProvider.ResetWebDriverToDefaultContent = this.ResetWebDriverToDefaultContent;
            return webElementProvider;
        }
    }
}