﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.WebElementProviders
{
    using System;
    using System.Collections.Generic;
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Provide Web Element by searching it inside the IFrame.
    /// </summary>
    public class WebElementProviderWithIframe : IWebElementProviderWithIframe
    {
        /// <summary>
        /// The Web Element provider.
        /// </summary>
        private IWebElementProvider webElementProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebElementProviderWithIframe" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public WebElementProviderWithIframe(ILogger logger)
        {
            this.Logger = logger;
            this.Logger.SetClassName(GetType().FullName);
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.webElementProvider.Name;
            }

            set
            {
                this.webElementProvider.Name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Web Driver.
        /// </summary>
        public IWebDriver WebDriver
        {
            get
            {
                return this.webElementProvider.WebDriver;
            }

            set
            {
                this.webElementProvider.WebDriver = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the WebDriver should be reset to default content before searching the element.
        /// </summary>
        public bool ResetWebDriverToDefaultContent
        {
            get
            {
                return this.webElementProvider.ResetWebDriverToDefaultContent;
            }

            set
            {
                this.webElementProvider.ResetWebDriverToDefaultContent = value;
            }
        }

        /// <summary>
        /// Gets the Web Element.
        /// </summary>
        public virtual IWebElement WebElement
        {
            get
            {
                if (this.SwitchToIframe())
                {
                    this.webElementProvider.ResetWebDriverToDefaultContent = false;
                    return this.webElementProvider.WebElement;
                } 
                else 
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the list of Web Elements.
        /// </summary>
        public virtual IList<IWebElement> WebElements
        {
            get
            {
                if (this.SwitchToIframe())
                {
                    this.webElementProvider.ResetWebDriverToDefaultContent = false;
                    return this.webElementProvider.WebElements;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the search expression.
        /// </summary>
        public string SearchExpression
        {
            get
            {
                return this.webElementProvider.SearchExpression;
            }

            set
            {
                this.webElementProvider.SearchExpression = value;
            }
        }

        /// <summary>
        /// Gets or sets the additional search parameters.
        /// </summary>
        public string[] SearchParameters
        {
            get
            {
                return this.webElementProvider.SearchParameters;
            }

            set
            {
                this.webElementProvider.SearchParameters = value;
            }
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Gets or sets the XPaths of parent IFrames.
        /// </summary>
        protected string[] ParentIframesXpaths { get; set; }

        /// <summary>
        /// Sets the Web Element provider.
        /// </summary>
        /// <param name="webElementProvider">The Web Element provider.</param>
        public void SetWebElementProvider(IWebElementProvider webElementProvider)
        {
            this.webElementProvider = webElementProvider;
        }

        /// <summary>
        /// Sets the XPaths for parent IFrames.
        /// </summary>
        /// <param name="parentIframeXPaths">The XPaths.</param>
        public void SetParentIframesXPaths(string[] parentIframeXPaths)
        {
            this.ParentIframesXpaths = parentIframeXPaths;
        }

        /// <summary>
        /// Clone current Web Element provider.
        /// </summary>
        /// <returns>New copy of current Web Element provider.</returns>
        public virtual IWebElementProvider Clone()
        {
            IWebElementProviderWithIframe webElementProviderWithIFrame = new WebElementProviderWithIframe(this.Logger);
            webElementProviderWithIFrame.SetWebElementProvider(this.webElementProvider.Clone());
            webElementProviderWithIFrame.SetParentIframesXPaths(this.ParentIframesXpaths);
            return webElementProviderWithIFrame;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>String representation of the current object.</returns>
        public override string ToString()
        {
            var parameters = new Dictionary<string, object>();

            for (int i = 0; i < this.ParentIframesXpaths.Length; i++)
            {
                parameters.Add("Iframe[" + i + "] xpath", this.ParentIframesXpaths[i]);
            }

            return LogFormatter.GetLog(parameters) + this.webElementProvider.ToString();
        }

        /// <summary>
        /// Switch to IFrame.
        /// </summary>
        /// <returns>Value that indicates whether switching to IFrame has ended successfully or not.</returns>
        protected bool SwitchToIframe()
        {
            this.WebDriver.SwitchTo().DefaultContent();

            for (int i = 0; i < this.ParentIframesXpaths.Length; i++)
            {
                string iframeXPath = this.ParentIframesXpaths[i];
                IWebElement iframeWebElement = null;

                try
                {
                    iframeWebElement = this.WebDriver.FindElement(By.XPath(iframeXPath));
                }
                catch (Exception ex)
                {
                    var parameters = new Dictionary<string, object>();
                    parameters.Add("Name", this.Name);
                    parameters.Add("Index", i);
                    parameters.Add("Search by", "xpath");
                    parameters.Add("Expression", iframeXPath);
                    this.Logger.Warn("Cannot find iframe\n{0}", LogFormatter.GetLog(parameters));
                    return false;
                }

                this.WebDriver.SwitchTo().Frame(iframeWebElement);
            }

            return true;
        }
    }
}
