﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Scroll to the current Web Component if it is not visible on the screen.
    /// </summary>
    public class ScrollAction : Action, IScrollAction
    {
        /// <summary>
        /// Scroll to the current Web Component if it is not visible on the screen.
        /// You can additionally define the top and bottom margins after scrolling (to avoid problems with interacting with element in case when you have a fixed header or footer on the page).
        /// </summary>
        /// <param name="topMargin">Indicates the distance between element and the top edge of the browser after scrolling.</param>
        /// <param name="bottomMargin">Indicates the distance between element and the bottom edge of the browser after scrolling.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool Scroll(int topMargin = 100, int bottomMargin = 100)
        {
            if (this.WebElement == null)
            {
                return false;
            }

            this.WebElement.ScrollToMargin(this.WebDriver, topMargin, bottomMargin);
            return true;
        }
    }
}
