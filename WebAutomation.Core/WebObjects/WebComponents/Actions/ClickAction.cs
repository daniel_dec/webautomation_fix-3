﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using OpenQA.Selenium;
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Click on the current Web Component (button / link).
    /// </summary>
    public class ClickAction : Action, IClickAction
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private ISettings settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClickAction" /> class.
        /// </summary>      
        /// <param name="settings">The settings.</param>
        public ClickAction(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Click the current Web Component (button / link).
        /// </summary>
        /// <param name="clickType">Defines the way how the Web Component should be clicked.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool Click(ClickType clickType = ClickType.Mouse)
        {
            if (this.WebElement == null)
            {
                return false;
            }

            switch (clickType)
            {
                case ClickType.Mouse:
                    this.WebElement.ScrollToMargin(this.WebDriver, this.settings.TopScrollMargin, this.settings.BottomScrollMargin);
                    this.WebElement.Click();
                    break;
                case ClickType.Right:
                    this.WebElement.ScrollToMargin(this.WebDriver, this.settings.TopScrollMargin, this.settings.BottomScrollMargin);
                    var actions = new OpenQA.Selenium.Interactions.Actions(this.WebDriver);
                    actions.ContextClick(this.WebElement);
                    actions.Perform();
                    break;
                case ClickType.Script:
                    IJavaScriptExecutor js = (IJavaScriptExecutor)this.WebDriver;

                    // js.ExecuteScript("$(arguments[0]).click();", this.WebElement);
                    js.ExecuteScript("arguments[0].click();", this.WebElement);
                    break;
            }

            return true;
        }
    }
}
