﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents
{
    using WebAutomation.Core.WebObjects.WebComponents.States;
    using WebAutomation.Core.WebObjects.WebComponents.Value;

    /// <summary>
    /// Assert that the current Web Component is in correct state or has a correct value.
    /// </summary>
    public class WebComponentAssert : IWebComponentAssert
    {
        #region Private fields

        /// <summary>
        /// Current state of Web Component.
        /// </summary>
        private IWebComponentAssertState stateIs;

        /// <summary>
        /// Future state of Web Component.
        /// </summary>
        private IWebComponentAssertState stateWillBe;

        /// <summary>
        /// Current value of Web Component.
        /// </summary>
        private IWebComponentAssertValue valueHas;

        /// <summary>
        ///  Future value of Web Component.
        /// </summary>
        private IWebComponentAssertValue valueWillHave;

        #endregion

        /// <summary>
        /// The Web Component.
        /// </summary>
        private IWebComponent webComponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentAssert" /> class.
        /// </summary>
        /// <param name="stateIs">Current state of Web Component.</param>
        /// <param name="stateWillBe">Future state of Web Component.</param>
        /// <param name="valueHas">Current value of Web Component.</param>
        /// <param name="valueWillHave">Future value of Web Component.</param>
        public WebComponentAssert(
            IWebComponentAssertState stateIs,
            IWebComponentAssertState stateWillBe,
            IWebComponentAssertValue valueHas,
            IWebComponentAssertValue valueWillHave)
        {
            this.stateIs = stateIs;
            this.stateWillBe = stateWillBe;
            this.valueHas = valueHas;
            this.valueWillHave = valueWillHave;
        }

        /// <summary>
        /// Assert that the current Web Component is in a correct state (for example is displayed on the page).
        /// </summary>
        public IAssertState Is
        {
            get
            {
                this.stateIs.SetCheckState(this.webComponent.Is);
                return this.stateIs;
            }
        }

        /// <summary>
        /// Assert that (few times) the current Web Component will be in a correct state (for example will be displayed on the page).
        /// You can define maximum number of attempts of determining the state in the test settings.
        /// </summary>
        public IAssertState WillBe
        {
            get
            {
                this.stateWillBe.SetCheckState(this.webComponent.WillBe);
                return this.stateWillBe;
            }
        }

        /// <summary>
        /// Gets the assertion of current value.
        /// </summary>
        public IAssertValue Has
        {
            get
            {
                this.valueHas.SetCheckValue(this.webComponent.Has);
                return this.valueHas;
            }
        }

        /// <summary>
        /// Gets the assertion of future value.
        /// </summary>
        public IAssertValue WillHave
        {
            get
            {
                this.valueWillHave.SetCheckValue(this.webComponent.WillHave);
                return this.valueWillHave;
            }
        }

        /// <summary>
        /// Set Web Component.
        /// </summary>
        /// <param name="webComponent">The Web Component.</param>
        public void SetWebComponent(IWebComponent webComponent)
        {
            this.webComponent = webComponent;
        }
    }
}
