﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.States
{
    using System.Collections.Generic;
    using System.Text;

    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Check whether the current Web Component is in a correct state (for example is displayed on the page).
    /// </summary>
    public class WebComponentCheckCurrentState : WebComponentOperation, IWebComponentCheckState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentCheckCurrentState" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        public WebComponentCheckCurrentState(ILogger logger, ISettings settings)
            : base(logger, settings)
        {
        }

        /// <summary>
        /// Gets or sets information about last action.
        /// </summary>
        public string LastActionInfo
        {
            get;
            protected set;
        }

        /// <summary>
        /// Check whether the current Web Component is present (in DOM tree).
        /// To check whether the Web Component is also displayed (correct CSS), please use Displayed() method instead.
        /// </summary>
        public virtual bool Present
        {
            get
            {
                IWebElement webElement = this.WebElementProvider.WebElement;
                bool result = webElement != null;
                this.UpdateLastActionInfo(result, "present");
                return result;
            }
        }

        /// <summary>
        /// Check whether the current Web Component is not present (in DOM tree).
        /// </summary>
        public virtual bool NotPresent
        {
            get
            {
                this.WebElementProvider.WebDriver.Manage().Timeouts().ImplicitlyWait(this.Settings.CheckIfNotPresentTimeout);
                IWebElement webElement = this.WebElementProvider.WebElement;
                this.WebElementProvider.WebDriver.Manage().Timeouts().ImplicitlyWait(this.Settings.CheckIfPresentTimeout);

                bool result = webElement == null;
                this.UpdateLastActionInfo(result, "not present");
                return result;
            }
        }

        /// <summary>
        /// Check whether the current Web Component is displayed on the page (present in DOM tree + correct CSS).
        /// If the Web Component is displayed, but is currently not visible in the web browser (for example scrolling is needed), the method will still return true.
        /// </summary>
        public virtual bool Displayed
        {
            get
            {
                IWebElement webElement = this.WebElementProvider.WebElement;
                bool result = webElement.IsDisplayed();
                this.UpdateLastActionInfo(result, "displayed");
                return result;
            }
        }

        /// <summary>
        /// Check whether the current Web Component is not displayed on the page (is not in DOM tree or has CSS values which makes him hidden).
        /// </summary>
        public virtual bool NotDisplayed
        {
            get
            {
                this.WebElementProvider.WebDriver.Manage().Timeouts().ImplicitlyWait(this.Settings.CheckIfNotPresentTimeout);
                IWebElement webElement = this.WebElementProvider.WebElement;
                this.WebElementProvider.WebDriver.Manage().Timeouts().ImplicitlyWait(this.Settings.CheckIfPresentTimeout);

                bool result = !webElement.IsDisplayed();
                this.UpdateLastActionInfo(result, "not displayed");
                return result;
            }
        }

        /// <summary>
        /// Check whether the checkbox is checked.
        /// </summary>
        public virtual bool Checked
        {
            get
            {
                IWebElement webElement = this.WebElementNotNull;
                bool result = webElement.Selected;
                this.UpdateLastActionInfo(result, "checked");
                return result;
            }
        }

        /// <summary>
        /// Check whether the checkbox is unchecked.
        /// </summary>
        public virtual bool NotChecked
        {
            get
            {
                IWebElement webElement = this.WebElementNotNull;
                bool result = !webElement.Selected;
                this.UpdateLastActionInfo(result, "not checked");
                return result;
            }
        }

        /// <summary>
        /// Check whether the Web Component is enabled.
        /// </summary>
        public virtual bool Enabled
        {
            get
            {
                IWebElement webElement = this.WebElementNotNull;
                bool result = webElement.Enabled;
                this.UpdateLastActionInfo(result, "enabled");
                return result;
            }
        }

        /// <summary>
        /// Check whether the Web Component is disabled.
        /// </summary>
        public virtual bool NotEnabled
        {
            get
            {
                IWebElement webElement = this.WebElementNotNull;
                bool result = !webElement.Enabled;
                this.UpdateLastActionInfo(result, "not enabled");
                return result;
            }
        }

        #region Protected Methods

        /// <summary>
        /// Update last action info.
        /// </summary>
        /// <param name="result">The result of action.</param>
        /// <param name="action">The name of action.</param>
        protected void UpdateLastActionInfo(bool result, string action)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("Result", result);
            
            StringBuilder info = new StringBuilder();
            info.AppendFormat("Verify whether the element is {0}\n", action);
            info.Append(LogFormatter.GetLog(parameters));
            info.Append(this.WebElementProvider.ToString());

            this.LastActionInfo = info.ToString();
            this.Logger.Info(this.LastActionInfo);
        }

        #endregion
    }
}
