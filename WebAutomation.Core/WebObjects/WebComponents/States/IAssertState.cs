﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.States
{
    /// <summary>
    /// Assert that the current Web Component is in a correct state (for example is displayed on the page).
    /// </summary>
    public interface IAssertState
    {
        /// <summary>
        /// Assert that the current Web Component is present (in DOM tree).
        /// To assert that the Web Component is also displayed (correct CSS), please use Displayed() method instead.
        /// </summary>
        void Present();

        /// <summary>
        /// Assert that the current Web Component is not present (in DOM tree).
        /// </summary>
        void NotPresent();

        /// <summary>
        /// Assert that the current Web Component is displayed on the page (present in DOM tree + correct CSS).
        /// If the Web Component is displayed, but is currently not visible in the web browser (for example scrolling is needed), the method will still return pass.
        /// </summary>
        void Displayed();

        /// <summary>
        /// Assert that the current Web Component is not displayed on the page (is not in DOM tree or has CSS values which makes him hidden).
        /// </summary>
        void NotDisplayed();

        /// <summary>
        /// Assert that the checkbox is checked.
        /// </summary>
        void Checked();

        /// <summary>
        /// Assert that the checkbox is unchecked.
        /// </summary>
        void NotChecked();

        /// <summary>
        /// Assert that the Web Component is enabled.
        /// </summary>
        void Enabled();

        /// <summary>
        /// Assert that the Web Component is disabled.
        /// </summary>
        void NotEnabled();
    }
}
