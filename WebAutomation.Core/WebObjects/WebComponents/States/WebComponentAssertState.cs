﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.States
{
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Assert that the current Web Component is in a correct state (for example is displayed on the page).
    /// </summary>
    public class WebComponentAssertState : IWebComponentAssertState
    {
        /// <summary>
        /// Check state.
        /// </summary>
        private ICheckState checkState;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentAssertState" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public WebComponentAssertState(ILogger logger)
        {
            this.Logger = logger;
            this.Logger.SetClassName(this.GetType().FullName);
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Assert that the current Web Component is present (in DOM tree).
        /// To assert that the Web Component is also displayed (correct CSS), please use Displayed() method instead.
        /// </summary>
        public void Present()
        {
            AssertHelper.AssertTest(this.checkState.Present, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the current Web Component is not present (in DOM tree).
        /// </summary>
        public void NotPresent()
        {
            AssertHelper.AssertTest(this.checkState.NotPresent, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the current Web Component is displayed on the page (present in DOM tree + correct CSS).
        /// If the Web Component is displayed, but is currently not visible in the web browser (for example scrolling is needed), the method will still return pass.
        /// </summary>
        public void Displayed()
        {
            AssertHelper.AssertTest(this.checkState.Displayed, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the current Web Component is not displayed on the page (is not in DOM tree or has CSS values which makes him hidden).
        /// </summary>
        public void NotDisplayed()
        {
            AssertHelper.AssertTest(this.checkState.NotDisplayed, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the checkbox is checked.
        /// </summary>
        public void Checked()
        {
            AssertHelper.AssertTest(this.checkState.Checked, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the checkbox is unchecked.
        /// </summary>
        public void NotChecked()
        {
            AssertHelper.AssertTest(this.checkState.NotChecked, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the Web Component is enabled.
        /// </summary>
        public void Enabled()
        {
            AssertHelper.AssertTest(this.checkState.Enabled, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the Web Component is disabled.
        /// </summary>
        public void NotEnabled()
        {
            AssertHelper.AssertTest(this.checkState.NotEnabled, this.checkState.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Set Check state.
        /// </summary>
        /// <param name="checkState">The check state.</param>
        public void SetCheckState(ICheckState checkState)
        {
            this.checkState = checkState;
        }
    }
}
