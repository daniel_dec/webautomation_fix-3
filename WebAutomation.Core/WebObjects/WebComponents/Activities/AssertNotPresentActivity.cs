﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Activities
{
    using System;
    using OpenQA.Selenium;
    using WebAutomation.Core.WebObjects.WebComponents.Attributes;

    /// <summary>
    /// Assert that Web Component is not present.
    /// </summary>
    public class AssertNotPresentActivity : IWebComponentActivity
    {
        /// <summary>
        /// Gets the type of the required attribute.
        /// In no attribute is required, null should be returned.
        /// </summary>
        public Type RequiredAttributeType
        {
            get
            {
                return typeof(AssertNotPresentAttribute);
            }
        }

        /// <summary>
        /// Perform the activity on given Web Component.
        /// </summary>
        /// <param name="webComponent">The Web Component.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="attributeValue">The value of attribute or null if attribute is no required.</param>
        public void Perform(IWebComponent webComponent, IWebDriver webDriver, string attributeValue = null)
        {
            webComponent.Assert.WillBe.NotPresent();
        }
    }
}
