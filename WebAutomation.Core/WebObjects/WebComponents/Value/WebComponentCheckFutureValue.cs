﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Value
{
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Check (few times) whether the current Web Component will have a correct value (for example text, CSS).
    /// You can define maximum number of attempts of determining the value in the test settings.
    /// </summary>
    public class WebComponentCheckFutureValue : WebComponentCheckCurrentValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentCheckFutureValue" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        public WebComponentCheckFutureValue(ILogger logger, ISettings settings)
            : base(logger, settings)
        {
        }

        /// <summary>
        /// Check whether the current Web Component (div, span, an element with text) will have a correct text displayed.
        /// To check the text inside a textbox, please use Value() method instead.
        /// </summary>
        /// <param name="text">The expected text on Web Component (div, span, an element with text).</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the text is correct.</returns>
        public override bool Text(string text, StringType stringComparisonType = StringType.Equals)
        {
            return this.WaitForExpectedResult(() => base.Text(text, stringComparisonType));
        }

        /// <summary>
        /// Check whether the textbox will have a correct value.
        /// </summary>
        /// <param name="value">The expected value in the textbox.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the value is correct.</returns>
        public override bool Value(string value, StringType stringComparisonType = StringType.Equals)
        {
            return this.WaitForExpectedResult(() => base.Value(value, stringComparisonType));
        }

        /// <summary>
        /// Check whether the current Web Component (div, span, an element with text) will have a text with given length.
        /// To check the length of text inside a textbox, please use ValueLength() method instead.
        /// </summary>
        /// <param name="length">The expected length of text in the Web Component (div, span, an element with text).</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        /// <returns>Value indicating whether the length is correct.</returns>
        public override bool TextLength(int length, NumberType numberComparisonType = NumberType.Equals)
        {
            return this.WaitForExpectedResult(() => base.TextLength(length, numberComparisonType));
        }

        /// <summary>
        /// Check whether the textbox will have a value with given length.
        /// </summary>
        /// <param name="length">The expected length of value in the textbox.</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        /// <returns>Value indicating whether the length is correct.</returns>
        public override bool ValueLength(int length, NumberType numberComparisonType = NumberType.Equals)
        {
            return this.WaitForExpectedResult(() => base.ValueLength(length, numberComparisonType));
        }

        /// <summary>
        /// Check whether the drop-down list will have a correct option.
        /// </summary>
        /// <param name="option">The expected option in drop-down list.</param>
        /// <param name="optionSelectionType">Defines the option state in a drop-down list.</param>
        /// <returns>Value indicating whether the option is correct.</returns>
        public override bool Option(string option, OptionType optionSelectionType = OptionType.Selected)
        {
            return this.WaitForExpectedResult(() => base.Option(option, optionSelectionType));
        }

        /// <summary>
        /// Check whether the current Web Component will have a CSS attribute with a given value.
        /// </summary>
        /// <param name="cssAttributeName">CSS attribute name.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the CSS value is correct.</returns>
        public override bool CssValue(string cssAttributeName, string value, StringType stringComparisonType = StringType.Equals)
        {
            return this.WaitForExpectedResult(() => base.CssValue(cssAttributeName, value, stringComparisonType));
        }

        /// <summary>
        /// Check whether the current Web Component will have a HTML attribute with a given value.
        /// </summary>
        /// <param name="htmlAttributeName">Name of html attribute.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the html attribute value is correct.</returns>
        public override bool AttributeValue(string htmlAttributeName, string value, StringType stringComparisonType = StringType.Equals)
        {
            return this.WaitForExpectedResult(() => base.AttributeValue(htmlAttributeName, value, stringComparisonType));
        }

        /// <summary>
        /// Check whether the elements found by the current Web Component will be equal to a given amount.
        /// </summary>
        /// <param name="number">Expected number of elements found by Web Component.</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        /// <returns>Value indicating whether the number of elements is correct.</returns>
        public override bool ElementsNumber(int number, NumberType numberComparisonType = NumberType.Equals)
        {
            return this.WaitForExpectedResult(() => base.ElementsNumber(number, numberComparisonType));
        }

        /// <summary>
        /// Check whether the elements found by the current Web Component will be sorted alphabetically by their text.
        /// </summary>
        /// <returns>Value indicating whether the elements found by Web Component are sorted correctly.</returns>
        public override bool ElementsSortedByText()
        {
            return this.WaitForExpectedResult(() => base.ElementsSortedByText());
        }
    }
}
