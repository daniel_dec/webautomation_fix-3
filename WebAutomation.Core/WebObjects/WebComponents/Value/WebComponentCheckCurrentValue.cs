﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Value
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Check whether the current Web Component has a correct value (for example text, CSS).
    /// </summary>
    public class WebComponentCheckCurrentValue : WebComponentOperation, IWebComponentCheckValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentCheckCurrentValue" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        public WebComponentCheckCurrentValue(ILogger logger, ISettings settings)
            : base(logger, settings)
        {
        }

        /// <summary>
        /// Gets or sets information about last action.
        /// </summary>
        public string LastActionInfo
        {
            get;
            protected set;
        }

        /// <summary>
        /// Check whether the current Web Component (div, span, an element with text) has a correct text displayed.
        /// To check the text inside a textbox, please use Value() method instead.
        /// </summary>
        /// <param name="text">The expected text on Web Component (div, span, an element with text).</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the text is correct.</returns>
        public virtual bool Text(string text, StringType stringComparisonType = StringType.Equals)
        {
            IWebElement webElement = this.WebElementNotNull;
            string webElementCurrentText = webElement.Text;
            bool result = this.CompareString(webElementCurrentText, text, stringComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", stringComparisonType);
            parameters.Add("Expected", text);
            parameters.Add("Actual", webElementCurrentText);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("text", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the textbox has a correct value.
        /// </summary>
        /// <param name="value">The expected value in the textbox.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the value is correct.</returns>
        public virtual bool Value(string value, StringType stringComparisonType = StringType.Equals)
        {
            IWebElement webElement = this.WebElementNotNull;
            string webElementCurrentValue = webElement.GetValue();
            bool result = this.CompareString(webElementCurrentValue, value, stringComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", stringComparisonType);
            parameters.Add("Expected", value);
            parameters.Add("Actual", webElementCurrentValue);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("value", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the current Web Component (div, span, an element with text) has a text with given length.
        /// To check the length of text inside a textbox, please use ValueLength() method instead.
        /// </summary>
        /// <param name="length">The expected length of text in the Web Component (div, span, an element with text).</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        /// <returns>Value indicating whether the length is correct.</returns>
        public virtual bool TextLength(int length, NumberType numberComparisonType = NumberType.Equals)
        {
            IWebElement webElement = this.WebElementNotNull;
            int webElementCurrentTextLength = webElement.Text.Length;
            bool result = this.CompareNumbers(webElementCurrentTextLength, length, numberComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", numberComparisonType);
            parameters.Add("Actual", webElementCurrentTextLength);
            parameters.Add("Expected", length);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("text length", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the textbox has a value with given length.
        /// </summary>
        /// <param name="length">The expected length of value in the textbox.</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        /// <returns>Value indicating whether the length is correct.</returns>
        public virtual bool ValueLength(int length, NumberType numberComparisonType = NumberType.Equals)
        {
            IWebElement webElement = this.WebElementNotNull;
            int webElementCurrentValueLength = webElement.GetValue().Length;
            bool result = this.CompareNumbers(webElementCurrentValueLength, length, numberComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", numberComparisonType);
            parameters.Add("Actual", webElementCurrentValueLength);
            parameters.Add("Expected", length);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("value length", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the drop-down list has a correct option.
        /// </summary>
        /// <param name="option">The expected option in drop-down list.</param>
        /// <param name="optionSelectionType">Defines the option state in a drop-down list.</param>
        /// <returns>Value indicating whether the option is correct.</returns>
        public virtual bool Option(string option, OptionType optionSelectionType = OptionType.Selected)
        {
            bool result = false;
            IWebElement selectedOption = null;
            string webElementCurrentSelection;

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", optionSelectionType);
            parameters.Add("Option", option);

            IWebElement webElement = this.WebElementNotNull;
            switch (optionSelectionType)
            {
                case OptionType.Selected:
                    selectedOption = webElement.SelectedOption();
                    webElementCurrentSelection = selectedOption == null ? string.Empty : selectedOption.Text;
                    result = webElementCurrentSelection.Equals(option);
                    parameters.Add("Actual", selectedOption == null ? string.Empty : selectedOption.Text);
                    break;
                case OptionType.NotSelected:
                    selectedOption = webElement.SelectedOption();
                    webElementCurrentSelection = selectedOption == null ? string.Empty : selectedOption.Text;
                    result = !webElementCurrentSelection.Equals(option);
                    parameters.Add("Actual", selectedOption == null ? string.Empty : selectedOption.Text);
                    break;
                case OptionType.Available:
                    result = webElement.GetOptions().Any(o => o.Text.Equals(option));
                    break;
                case OptionType.NotAvailable:
                    result = !webElement.GetOptions().Any(o => o.Text.Equals(option));
                    break;
            }

            parameters.Add("Result", result);
            this.UpdateLastActionInfo("option", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the current Web Component has a CSS attribute with a given value.
        /// </summary>
        /// <param name="cssAttributeName">CSS attribute name.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the CSS value is correct.</returns>
        public virtual bool CssValue(string cssAttributeName, string value, StringType stringComparisonType = StringType.Equals)
        {
            IWebElement webElement = this.WebElementNotNull;
            string webElementCurrentCssValue = webElement.GetCssValue(cssAttributeName);
            bool result = this.CompareString(webElementCurrentCssValue, value, stringComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", stringComparisonType);
            parameters.Add("Attribute", cssAttributeName);
            parameters.Add("Actual", webElementCurrentCssValue);
            parameters.Add("Expected", value);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("css attrbiute with given value", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the current Web Component has a HTML attribute with a given value.
        /// </summary>
        /// <param name="htmlAttributeName">Name of html attribute.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        /// <returns>Value indicating whether the html attribute value is correct.</returns>
        public virtual bool AttributeValue(string htmlAttributeName, string value, StringType stringComparisonType = StringType.Equals)
        {
            IWebElement webElement = this.WebElementNotNull;
            string webElementCurrentAttributeValue = webElement.GetAttribute(htmlAttributeName);
            bool result = this.CompareString(webElementCurrentAttributeValue, value, stringComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", stringComparisonType);
            parameters.Add("Attribute", htmlAttributeName);
            parameters.Add("Actual", webElementCurrentAttributeValue);
            parameters.Add("Expected", value);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("attrbiute with given value", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the elements found by the current Web Component are equal to a given amount.
        /// </summary>
        /// <param name="number">Expected number of elements found by Web Component.</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        /// <returns>Value indicating whether the number of elements is correct.</returns>
        public virtual bool ElementsNumber(int number, NumberType numberComparisonType = NumberType.Equals)
        {
            IList<IWebElement> webElements = this.WebElementProvider.WebElements;
            int webElementsCurrentNumber = webElements.Count;
            bool result = this.CompareNumbers(webElementsCurrentNumber, number, numberComparisonType);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", numberComparisonType);
            parameters.Add("Actual", webElementsCurrentNumber);
            parameters.Add("Expected", number);
            parameters.Add("Result", result);
            this.UpdateLastActionInfo("multiple instances", parameters);

            return result;
        }

        /// <summary>
        /// Check whether the elements found by the current Web Component are sorted alphabetically by their text.
        /// </summary>
        /// <returns>Value indicating whether the elements found by Web Component are sorted correctly.</returns>
        public virtual bool ElementsSortedByText()
        {
            IList<IWebElement> webElements = this.WebElementProvider.WebElements;
            var webElementsText = from webElement
                                  in webElements
                                  select webElement.Text;
            var webElementsSortedText = webElementsText.OrderBy(e => e);

            int incorrectElementIndex = -1;
            for (int i = 0; i < webElementsText.Count(); i++)
            {
                if (webElementsText.ElementAt(i) != webElementsSortedText.ElementAt(i))
                {
                    incorrectElementIndex = i;
                    break;
                }
            }

            bool result = incorrectElementIndex == -1;

            var parameters = new Dictionary<string, object>();
            if (!result)
            {   
                parameters.Add("Actual[" + incorrectElementIndex + "]", webElementsText.ElementAt(incorrectElementIndex));
                parameters.Add("Expected[" + incorrectElementIndex + "]", webElementsSortedText.ElementAt(incorrectElementIndex));
            }

            parameters.Add("Result", result);
            this.UpdateLastActionInfo("items sorted by text", parameters);

            return result;
        }

        #region Protected Methods

        /// <summary>
        /// Update last action info.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="parameters">The parameters.</param>
        protected void UpdateLastActionInfo(string entity, Dictionary<string, object> parameters)
        {
            StringBuilder info = new StringBuilder();

            info.AppendFormat("Verify whether the element has {0}\n", entity);
            info.Append(LogFormatter.GetLog(parameters));
            info.Append(this.WebElementProvider.ToString());
            
            this.LastActionInfo = info.ToString();
            this.Logger.Info(this.LastActionInfo);
        }

        /// <summary>
        /// Compare two strings.
        /// </summary>
        /// <param name="currentValue">The current value.</param>
        /// <param name="expectedValue">The expected value.</param>
        /// <param name="stringComparisonType">Comparison type.</param>
        /// <returns>Value indicating whether the value is correct.</returns>
        protected bool CompareString(string currentValue, string expectedValue, StringType stringComparisonType)
        {
            currentValue = currentValue == null ? string.Empty : currentValue;
            expectedValue = expectedValue == null ? string.Empty : expectedValue;
            bool result = false;

            switch (stringComparisonType)
            {
                case StringType.Equals:
                    result = currentValue.Equals(expectedValue);
                    break;
                case StringType.NotEquals:
                    result = !currentValue.Equals(expectedValue);
                    break;
                case StringType.Contains:
                    result = currentValue.Contains(expectedValue);
                    break;
                case StringType.NotContains:
                    result = !currentValue.Contains(expectedValue);
                    break;
                case StringType.StartsWith:
                    result = currentValue.StartsWith(expectedValue);
                    break;
                case StringType.EndsWith:
                    result = currentValue.EndsWith(expectedValue);
                    break;
            }

            return result;
        }

        /// <summary>
        /// Compare two numbers.
        /// </summary>
        /// <param name="currentValue">The current value.</param>
        /// <param name="expectedValue">The expected value.</param>
        /// <param name="numberComparisonType">Comparison type.</param>
        /// <returns>Value indicating whether the value is correct.</returns>
        protected bool CompareNumbers(int currentValue, int expectedValue, NumberType numberComparisonType)
        {
            bool result = false;

            switch (numberComparisonType)
            {
                case NumberType.Equals:
                    result = currentValue == expectedValue;
                    break;
                case NumberType.NotEquals:
                    result = currentValue != expectedValue;
                    break;
                case NumberType.LessThan:
                    result = currentValue < expectedValue;
                    break;
                case NumberType.LessThanOrEqualTo:
                    result = currentValue <= expectedValue;
                    break;
                case NumberType.GreaterThan:
                    result = currentValue > expectedValue;
                    break;
                case NumberType.GreaterThanOrEqualTo:
                    result = currentValue >= expectedValue;
                    break;
            }

            return result;
        }

        #endregion
    }
}
