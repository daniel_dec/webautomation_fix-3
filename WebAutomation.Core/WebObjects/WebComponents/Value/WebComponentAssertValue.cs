﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Value
{
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Assert that the current Web Component has a correct value (for example text, CSS).
    /// </summary>
    public class WebComponentAssertValue : IWebComponentAssertValue
    {
        /// <summary>
        /// Check value.
        /// </summary>
        private ICheckValue checkValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentAssertValue" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public WebComponentAssertValue(ILogger logger)
        {
            this.Logger = logger;
            this.Logger.SetClassName(this.GetType().FullName);
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Assert that the current Web Component (div, span, an element with text) has a correct text displayed.
        /// To assert the text inside a textbox, please use Value() method instead.
        /// </summary>
        /// <param name="text">The expected text on Web Component (div, span, an element with text).</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        public void Text(string text, StringType stringComparisonType = StringType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.Text(text, stringComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the textbox has a correct value.
        /// </summary>
        /// <param name="value">The expected value in the textbox.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        public void Value(string value, StringType stringComparisonType = StringType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.Value(value, stringComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the current Web Component (div, span, an element with text) has a text with given length.
        /// To assert the length of text inside a textbox, please use ValueLength() method instead.
        /// </summary>
        /// <param name="length">The expected length of text in the Web Component (div, span, an element with text).</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        public void TextLength(int length, NumberType numberComparisonType = NumberType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.TextLength(length, numberComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the textbox has a value with given length.
        /// </summary>
        /// <param name="length">The expected length of value in the textbox.</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        public void ValueLength(int length, NumberType numberComparisonType = NumberType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.ValueLength(length, numberComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the drop-down list has a correct option.
        /// </summary>
        /// <param name="option">The expected option in drop-down list.</param>
        /// <param name="optionSelectionType">Defines the option state in a drop-down list.</param>
        public void Option(string option, OptionType optionSelectionType = OptionType.Selected)
        {
            AssertHelper.AssertTest(this.checkValue.Option(option, optionSelectionType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the current Web Component has a CSS attribute with a given value.
        /// </summary>
        /// <param name="cssAttributeName">CSS attribute name.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        public void CssValue(string cssAttributeName, string value, StringType stringComparisonType = StringType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.CssValue(cssAttributeName, value, stringComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the current Web Component has a HTML attribute with a given value.
        /// </summary>
        /// <param name="htmlAttributeName">Name of html attribute.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="stringComparisonType">Defines the way how to compare two strings.</param>
        public void AttributeValue(string htmlAttributeName, string value, StringType stringComparisonType = StringType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.AttributeValue(htmlAttributeName, value, stringComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the elements found by the current Web Component are equal to a given amount.
        /// </summary>
        /// <param name="number">Expected number of elements found by Web Component.</param>
        /// <param name="numberComparisonType">Defines the way how to compare two numbers.</param>
        public void ElementsNumber(int number, NumberType numberComparisonType = NumberType.Equals)
        {
            AssertHelper.AssertTest(this.checkValue.ElementsNumber(number, numberComparisonType), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Assert that the elements found by the current Web Component are sorted alphabetically by their text.
        /// </summary>
        public void ElementsSortedByText()
        {
            AssertHelper.AssertTest(this.checkValue.ElementsSortedByText(), this.checkValue.LastActionInfo, this.Logger);
        }

        /// <summary>
        /// Set check value.
        /// </summary>
        /// <param name="checkValue">The check value.</param>
        public void SetCheckValue(ICheckValue checkValue)
        {
            this.checkValue = checkValue;
        }
    }
}
