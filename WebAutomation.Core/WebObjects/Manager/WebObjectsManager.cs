﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.Common.Activities;
    using WebAutomation.Core.WebObjects.Common.Attributes;
    using WebAutomation.Core.WebObjects.WebComponents;
    using WebAutomation.Core.WebObjects.WebComponents.Activities;
    using WebAutomation.Core.WebObjects.WebContainer.Activities;

    /// <summary>
    /// Manager class for Web Objects.
    /// </summary>
    internal class WebObjectsManager : IWebObjectsManager
    {
        /// <summary>
        /// The activity resolver.
        /// </summary>
        private IActivityResolver activityResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebObjectsManager" /> class.
        /// </summary>
        /// <param name="activityResolver">The activity resolver.</param>
        /// <param name="logger">The logger.</param>
        public WebObjectsManager(IActivityResolver activityResolver, ILogger logger)
        {
            this.activityResolver = activityResolver;

            this.Logger = logger;
            this.Logger.SetClassName(GetType().FullName);
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Create the Web Container and execute all registered activities.
        /// </summary>
        /// <typeparam name="TWebContainer">Type of Web Container.</typeparam>
        /// <param name="webDriver">The Web Driver.</param>
        /// <returns>Loaded Web Container.</returns>
        public TWebContainer Load<TWebContainer>(IWebDriver webDriver) where TWebContainer : new()
        {
            return (TWebContainer)this.Load(typeof(TWebContainer), webDriver);
        }

        /// <summary>
        /// Create the Web Container and execute all registered activities.
        /// </summary>
        /// <param name="webContainerType">Type of Web Container.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <returns>Loaded Web Container.</returns>
        public object Load(Type webContainerType, IWebDriver webDriver)
        {
            AssertHelper.AssertUsage(webDriver != null, "Cannot create WebContainer because WebDriver is null", this.Logger);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Name", webContainerType.Name);

            this.Logger.Debug("Load the Web Container\n{0}", LogFormatter.GetLog(parameters));
            object webContainer = Activator.CreateInstance(webContainerType);
            this.PerformActivities(webContainer, webDriver);
            return webContainer;
        }

        #region Protected methods

        /// <summary>
        /// Perform activities.
        /// </summary>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webDriver">The Web Driver.</param>
        protected void PerformActivities(object webContainer, IWebDriver webDriver)
        {
            var webComponentProperties = webContainer
                .GetType()
                .GetProperties()
                .Where(p => p.PropertyType.Equals(typeof(IWebComponent)))
                .ToList();

            var webContainerAttributesTypes = webContainer
                .GetType()
                .GetCustomAttributes(true)
                .Select(ca => ca.GetType())
                .ToList();

            var activities = this.activityResolver.GetRegisteredActivities();
            foreach (var activity in activities)
            {
                var activityType = this.activityResolver.Get<IActivity>(activity).GetType();
                if (typeof(IOnActionActivity).IsAssignableFrom(activityType))
                {
                    this.InitializeOnActionAcitivities(activity, webContainer, webComponentProperties);
                    continue;
                }

                if (typeof(IWebContainerActivity).IsAssignableFrom(activityType))
                {
                    this.PerformWebContainerActivities(activity, webContainer, webDriver, webContainerAttributesTypes);
                }

                if (typeof(IWebComponentActivity).IsAssignableFrom(activityType))
                {
                    this.PerformWebComponentActivities(activity, webContainer, webDriver, webComponentProperties);
                }
            }
        }

        /// <summary>
        /// Initialize OnAction activities.
        /// </summary>
        /// <param name="activity">The activity name.</param>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webComponentProperties">Web Component properties.</param>
        protected void InitializeOnActionAcitivities(string activity, object webContainer, IList<PropertyInfo> webComponentProperties)
        {
            var onActionActivity = this.activityResolver.Get<IOnActionActivity>(activity);
            var webComponentsWithAttributes = this.GetFilteredWebComponents(webContainer, webComponentProperties, onActionActivity.RequiredAttributeType);
            
            foreach (var webComponentWithAttribute in webComponentsWithAttributes)
            {
                var webComponent = webComponentWithAttribute.Item1;
                var attributeValue = webComponentWithAttribute.Item2;

                onActionActivity = this.activityResolver.Get<IOnActionActivity>(activity);
                onActionActivity.AttributeValue = attributeValue;
                webComponent.Perform.RegisterActivity(onActionActivity);

                onActionActivity = this.activityResolver.Get<IOnActionActivity>(activity);
                onActionActivity.AttributeValue = attributeValue;
                webComponent.PerformIfExists.RegisterActivity(onActionActivity);
            }
        }

        /// <summary>
        /// Perform all Web Component activities on given Web Container.
        /// </summary>
        /// <param name="activity">The activity name.</param>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="webComponentProperties">Web Component properties.</param>
        protected void PerformWebComponentActivities(string activity, object webContainer, IWebDriver webDriver, IList<PropertyInfo> webComponentProperties)
        {
            var webComponentActivity = this.activityResolver.Get<IWebComponentActivity>(activity);
            var webComponentsWithAttributes = this.GetFilteredWebComponents(webContainer, webComponentProperties, webComponentActivity.RequiredAttributeType);

            foreach (var webComponentWithAttribute in webComponentsWithAttributes)
            {
                var webComponent = webComponentWithAttribute.Item1;
                var attributeValue = webComponentWithAttribute.Item2;

                webComponentActivity = this.activityResolver.Get<IWebComponentActivity>(activity);
                webComponentActivity.Perform(webComponent, webDriver, attributeValue);
            }
        }

        /// <summary>
        /// Perform activities on given Web Container.
        /// </summary>
        /// <param name="activity">The activity name.</param>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="containerAttributeTypes">The Web Container attribute types.</param>
        protected void PerformWebContainerActivities(string activity, object webContainer, IWebDriver webDriver, IList<Type> containerAttributeTypes)
        {
            var webContainerActivity = this.activityResolver.Get<IWebContainerActivity>(activity);
            if (webContainerActivity.RequiredAttributeType == null)
            {
                // Perform activity on web container.
                webContainerActivity.Perform(webContainer, webDriver, string.Empty);
            }
            else
            {
                // Perform activity on web container only if it contains (or his parent class) specified attribute.
                if (containerAttributeTypes.Contains(webContainerActivity.RequiredAttributeType))
                {
                    string attributeValue = AttributeHelper.GetAttributeValue(webContainerActivity.RequiredAttributeType, webContainer.GetType());
                    webContainerActivity.Perform(webContainer, webDriver, attributeValue);
                }
            }
        }

        /// <summary>
        /// Get filtered Web Components.
        /// </summary>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webComponentProperties">Web Component properties.</param>
        /// <param name="requiredAttributeType">The required attribute type.</param>
        /// <returns>List of Web Components with attributes.</returns>
        private IList<Tuple<IWebComponent, string>> GetFilteredWebComponents(object webContainer, IList<PropertyInfo> webComponentProperties, Type requiredAttributeType = null)
        {
            var filteredWebComponentsPropertyInfo = webComponentProperties;
            if (requiredAttributeType != null)
            {
                filteredWebComponentsPropertyInfo = webComponentProperties
                    .Where(p => p.CustomAttributes.Any(ca => ca.AttributeType.Equals(requiredAttributeType)))
                    .ToList();
            }
            
            return filteredWebComponentsPropertyInfo
                .Select(pi => new Tuple<IWebComponent, string>(
                    (IWebComponent)pi.GetValue(webContainer), 
                    AttributeHelper.GetAttributeValue(requiredAttributeType, pi)))
                .ToList();
        }

        #endregion
    }
}
