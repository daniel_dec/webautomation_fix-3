﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.Manager
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Activities;
    using Microsoft.Practices.Unity;

    /// <summary>
    /// Resolve registered activities.
    /// </summary>
    internal class ActivityResolver : IActivityResolver
    {
        /// <summary>
        /// The container.
        /// </summary>
        private IUnityContainer container;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityResolver" /> class.
        /// </summary>
        /// <param name="container">The dependency container.</param>
        public ActivityResolver(IUnityContainer container)
        {
            this.container = container;
        }

        /// <summary>
        /// Get the names of registered activities.
        /// </summary>
        /// <returns>Registered activities.</returns>
        public IList<string> GetRegisteredActivities()
        {
            return this.container
                .Registrations
                .Where(r => r.RegisteredType == typeof(IActivity))
                .Select(r => r.Name)
                .ToList();
        }

        /// <summary>
        /// Get instance of activity with the given type.
        /// </summary>
        /// <typeparam name="TActivity">Type of activity.</typeparam>
        /// <param name="name">The name of registered activity.</param>
        /// <returns>The activity.</returns>
        public TActivity Get<TActivity>(string name) where TActivity : IActivity
        {
            return (TActivity)this.container.Resolve<IActivity>(name);
        }
    }
}
