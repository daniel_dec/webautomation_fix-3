﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebContainer.Activities
{
    using System;
    using System.Reflection;
    using OpenQA.Selenium;
    using WebAutomation.Core.WebObjects.Common.Attributes;
    using WebAutomation.Core.WebObjects.WebComponents;

    /// <summary>
    /// Create all Web Components in the Web Container.
    /// </summary>
    public class CreateWebComponentsActivity : IWebContainerActivity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateWebComponentsActivity" /> class.
        /// </summary>
        /// <param name="webComponentResolver">The Web Component resolver.</param>
        public CreateWebComponentsActivity(Func<IWebComponent> webComponentResolver)
        {
            this.WebComponentResolver = webComponentResolver;
        }

        /// <summary>
        /// Gets the type of the required attribute.
        /// In no attribute is required, null should be returned.
        /// </summary>
        public Type RequiredAttributeType
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the resolver for Web Components.
        /// </summary>
        protected Func<IWebComponent> WebComponentResolver { get; set; }

        /// <summary>
        /// Perform the activity on given Web Container.
        /// </summary>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="attributeValue">The value of attribute or null if attribute is no required.</param>
        public void Perform(object webContainer, IWebDriver webDriver, string attributeValue = null)
        {
            var webComponentsPropertyInfo = WebContainerHelper.GetAllWebComponentsPropertyInfo(webContainer.GetType());
            foreach (PropertyInfo webComponentPropertyInfo in webComponentsPropertyInfo)
            {
                // Create new instance of Web Component
                var webComponent = this.WebComponentResolver.Invoke();
                webComponentPropertyInfo.SetValue(webContainer, webComponent);

                // Update properties
                var attributes = AttributeHelper.GetAttributes(webComponentPropertyInfo);
                foreach (var attr in attributes)
                {
                    // Calculate friendly name for key
                    string key = attr
                                  .Key
                                  .Substring(0, attr.Key.LastIndexOf("Attribute"))
                                  .ToLower();

                    webComponent.Properties.Add(key, attr.Value);
                }
                
                // Fullname property
                string webContainerName = webContainer.GetType().Name;
                string webComponentName = webComponent.Properties["name"];
                webComponent.Properties.Add("fullname", string.Format("{0}.{1}", webContainerName, webComponentName));
            }
        }
    }
}
