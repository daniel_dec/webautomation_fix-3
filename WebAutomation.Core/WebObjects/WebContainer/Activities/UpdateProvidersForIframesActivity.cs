﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebContainer.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using WebAutomation.Core.WebObjects.Common.Attributes;
    using WebAutomation.Core.WebObjects.WebComponents;
    using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;
    using WebAutomation.Core.WebObjects.WebContainer.Attributes;

    /// <summary>
    /// Update Web Element provider to handle IFrames.
    /// </summary>
    public class UpdateProvidersForIframesActivity : IWebContainerActivity
    {
        /// <summary>
        /// Resolver for Web Element provider with IFrames.
        /// </summary>
        private Func<IWebElementProviderWithIframe> webElementProviderWithIframesResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateProvidersForIframesActivity" /> class.
        /// </summary>
        /// <param name="webElementProviderWithIframesResolver">The resolver for Web Element provider with IFrames.</param>
        public UpdateProvidersForIframesActivity(Func<IWebElementProviderWithIframe> webElementProviderWithIframesResolver)
        {
            this.webElementProviderWithIframesResolver = webElementProviderWithIframesResolver;
        }

        /// <summary>
        /// Gets the type of the required attribute.
        /// In no attribute is required, null should be returned.
        /// </summary>
        public Type RequiredAttributeType
        {
            get
            {
                return typeof(IframeAttribute);
            }
        }

        /// <summary>
        /// Perform the activity on given Web Container.
        /// </summary>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="attributeValue">The value of attribute or null if attribute is no required.</param>
        public void Perform(object webContainer, IWebDriver webDriver, string attributeValue = null)
        {
            IList<string> iframeXPaths = new List<string>();
            this.ApplyIFrameXPathsToWebComponents(webContainer, webContainer.GetType(), iframeXPaths);
        }

        /// <summary>
        /// Apply IFrame provider to all Web Components defined in given web container.
        /// </summary>
        /// <param name="webContainer">Instance of Web Container.</param>
        /// <param name="webContainerType">Type of Web Container.</param>
        /// <param name="xpaths">List of XPaths to be applies on Web Components.</param>
        protected void ApplyIFrameXPathsToWebComponents(object webContainer, Type webContainerType, IList<string> xpaths)
        {
            // We must start from the top base class.
            Type webContainerBaseType = webContainerType.BaseType;
            if (webContainerBaseType != null)
            {
                this.ApplyIFrameXPathsToWebComponents(webContainer, webContainerBaseType, xpaths);
            }

            // Check if there is an iframe attribute for the class
            string newXPath = AttributeHelper.GetAttributeValue(this.RequiredAttributeType, webContainerType);
            if (!string.IsNullOrEmpty(newXPath))
            {
                xpaths.Add(newXPath);
            }

            if (xpaths.Count == 0)
            {
                return;
            }

            // Update all Web Components defined in given type
            string[] xpathsArray = xpaths.ToArray();
            var webComponents = WebContainerHelper.GetNonInheritetWebComponents(webContainer, webContainerType);
            foreach (IWebComponent webComponent in webComponents)
            {
                IWebElementProviderWithIframe webElementProviderWithIframe = this.webElementProviderWithIframesResolver.Invoke();
                webElementProviderWithIframe.SetWebElementProvider(webComponent.WebElementProvider);
                webElementProviderWithIframe.SetParentIframesXPaths(xpathsArray);

                webComponent.WebElementProvider = webElementProviderWithIframe;
            }
        }
    }
}
