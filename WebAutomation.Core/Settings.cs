﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System;

    /// <summary>
    /// Test settings.
    /// </summary>
    public class Settings : ISettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Settings" /> class.
        /// </summary>
        /// <param name="presentTimeoutInMilliseconds">The timeout (in milliseconds) for checking if element is present.</param>
        /// <param name="notPresentTimeoutInMilliseconds">The timeout (in milliseconds) for checking if element is not present.</param>
        /// <param name="topScrollMargin">The distance between element and the top edge of the browser after scrolling.</param>
        /// <param name="bottomScrollMargin">The distance between element and the bottom edge of the browser after scrolling.</param>
        /// <param name="waitForExpectedResultAttempts">The number of attempts during checking for expected result in WillBe and WillHave actions.</param>
        /// <param name="waitForExpectedResultsSleepTime">the sleep time (in milliseconds) before next attempt of checking the results in WillBe and WillHave actions.</param>
        public Settings(
            int presentTimeoutInMilliseconds, 
            int notPresentTimeoutInMilliseconds,
            int topScrollMargin,
            int bottomScrollMargin,
            int waitForExpectedResultAttempts,
            int waitForExpectedResultsSleepTime)
        {
            this.CheckIfPresentTimeout = TimeSpan.FromMilliseconds(presentTimeoutInMilliseconds);
            this.CheckIfNotPresentTimeout = TimeSpan.FromMilliseconds(notPresentTimeoutInMilliseconds);

            this.TopScrollMargin = topScrollMargin;
            this.BottomScrollMargin = bottomScrollMargin;

            this.WaitForExpectedResultAttempts = waitForExpectedResultAttempts;
            this.WaitForExpectedResultsSleepTime = TimeSpan.FromMilliseconds(waitForExpectedResultsSleepTime);
        }

        /// <summary>
        /// Gets the timeout used when checking if element is present.
        /// </summary>
        public TimeSpan CheckIfPresentTimeout { get; private set; }

        /// <summary>
        /// Gets the timeout used when checking if element is not present.
        /// </summary>
        public TimeSpan CheckIfNotPresentTimeout { get; private set; }

        /// <summary>
        /// Gets the distance between element and the top edge of the browser after scrolling.
        /// </summary>
        public int TopScrollMargin { get; private set; }

        /// <summary>
        /// Gets the distance between element and the bottom edge of the browser after scrolling.
        /// </summary>
        public int BottomScrollMargin { get; private set; }

        /// <summary>
        /// Gets the number of attempts during checking for expected result in WillBe an WillHave actions.
        /// </summary>
        public int WaitForExpectedResultAttempts { get; private set;  }

        /// <summary>
        /// Gets the sleep time before next attempt of checking the results in WillBe and WillHave actions.
        /// </summary>
        public TimeSpan WaitForExpectedResultsSleepTime { get; private set; }
    }
}
