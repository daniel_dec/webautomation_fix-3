﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System.Collections.Generic;
    using OpenQA.Selenium;

    /// <summary>
    /// The session manager allows you to use multiple Web Drivers during a test.
    /// </summary>
    public interface ISessionManager
    {  
        /// <summary>
        /// Gets the available sessions.
        /// </summary>
        IReadOnlyList<string> AvailableSessionsNames { get; }

        /// <summary>
        /// Gets the name of current session.
        /// </summary>
        string CurrentSessionName { get; }

        /// <summary>
        /// Gets or sets the WebDriver for the current session.
        /// </summary>
        IWebDriver CurrentSession { get; set; }

        /// <summary>
        /// Switch current session to the default session.
        /// </summary>
        void SwitchToDefaultSession();

        /// <summary>
        /// Switch current session to the session with the given name.
        /// </summary>
        /// <param name="sessionName">The name of session.</param>
        /// <param name="createIfDoesntExist">Create a new session if session with the given name doesn't exist.</param>
        void SwitchSession(string sessionName, bool createIfDoesntExist = true);
    }
}
