﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.SpecFlow
{
    using System;
    using System.Linq;
    using System.Reflection;
    
    using TechTalk.SpecFlow;
    using WebAutomation.Core;
    using WebAutomation.Core.WebObjects.WebComponents;

    /// <summary>
    /// Base class for SpecFlow tests.
    /// </summary>
    public class SpecFlowTestBase : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecFlowTestBase" /> class.
        /// </summary>
        /// <param name="scenarioContext">The scenario context.</param>
        public SpecFlowTestBase(ScenarioContext scenarioContext)
            : base(false)
        {
            this.ScenarioContext = scenarioContext;
            this.InitializeSessionManager();
        }

        #region Public properties

        /// <summary>
        /// Scenario Context.
        /// </summary>
        public virtual ScenarioContext ScenarioContext { get; set; }

        /// <summary>
        /// Gets or sets the Current Web Container.
        /// </summary>
        public override object CurrentWebContainer
        {
            get
            {
                return this.ScenarioContext.GetValue<object>("CurrentWebContainer");
            }

            protected set
            {
                this.ScenarioContext.SetValue("CurrentWebContainer", value);
            }
        }

        /// <summary>
        /// Gets or sets the Session Manager.
        /// </summary>
        public override ISessionManager SessionManager
        {
            get
            {
                return this.ScenarioContext.GetValue<ISessionManager>("SessionManager");
            }

            protected set
            {
                this.ScenarioContext.SetValue("SessionManager", value);
            }
        }

        /// <summary>
        /// Gets the assembly where Web Objects are located.
        /// </summary>
        protected virtual Assembly WebObjectsAssembly
        {
            get
            {
                return null;
            }
        }

        #endregion

        #region Web Component

        /// <summary>
        /// Get Web Component by name.
        /// Important: property 'WebObjectsAssembly' must return the assembly where the Container is located.
        /// To use correct assembly, please override 'WebObjectsAssembly' property with 'Assembly.GetAssembly(typeof(YourClass))'.
        /// </summary>
        /// <param name="webContainerName">The name of container that contains component.</param>
        /// <param name="webComponentName">The name of component.</param>
        /// <returns>The Web Component.</returns>
        public IWebComponent GetWebComponent(string webContainerName, string webComponentName)
        {
            AssertHelper.AssertUsage(!string.IsNullOrEmpty(webContainerName), "Web Container name cannot be empty", this.Logger);
            AssertHelper.AssertUsage(!string.IsNullOrEmpty(webComponentName), "Web Component name cannot be empty", this.Logger);

            Assembly webObjectsAssembly = this.WebObjectsAssembly != null ? this.WebObjectsAssembly : Assembly.GetCallingAssembly();
            this.UpdateCurrentWebContainer(webContainerName, webObjectsAssembly);
            return this.GetWebComponentByReflection(this.CurrentWebContainer, webComponentName);
        }

        /// <summary>
        /// Update current Web Container.
        /// </summary>
        /// <param name="webContainerName">The name of Web Container.</param>
        /// <param name="webObjectsAssembly">The assembly where Web Objects are located.</param>
        private void UpdateCurrentWebContainer(string webContainerName, Assembly webObjectsAssembly)
        {
            Type webContainerType = webObjectsAssembly.ExportedTypes.FirstOrDefault(type => type.Name.Equals(webContainerName));

            string errorMessage = string.Format(
                    "Cannot find WebContainer '{0}' in given assembly '{1}'. Please ensure that both WebContainer name and returned assembly in 'WebObjectsAssembly' property are correct (if not, please override it in your subclass).",
                    webContainerName,
                    webObjectsAssembly.FullName);

            AssertHelper.AssertUsage(
                webContainerType != null,
                errorMessage,
                this.Logger);

            this.GetContainer(webContainerType);
        }

        /// <summary>
        /// Get Web Component inside given Web Container.
        /// </summary>
        /// <param name="webContainer">The Web Container.</param>
        /// <param name="webComponentName">The Web Component name.</param>
        /// <returns>The Web Component.</returns>
        private IWebComponent GetWebComponentByReflection(object webContainer, string webComponentName)
        {
            Type webContaienrType = webContainer.GetType();
            PropertyInfo webComponentPropertyInfo = webContaienrType.GetProperty(webComponentName, typeof(IWebComponent));

            string errorMessage = string.Format(
                    "Cannot find WebComponent '{0}' inside '{1}' class. Please ensure that the name of WebComponent is correct and exists in given WebContainer.",
                    webComponentName,
                    webContaienrType.Name);

            AssertHelper.AssertUsage(
                webComponentPropertyInfo != null,
                errorMessage,
                this.Logger);

            return (IWebComponent)webComponentPropertyInfo.GetValue(webContainer);
        }

        #endregion
    }
}
