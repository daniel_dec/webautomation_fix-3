﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using System;
    using System.IO;
    using System.Reflection;
    using NUnit.Framework;
    using WebAutomation.Core.Tests.PageDefinitions;
    using WebAutomation.Core.WebObjects.Common;
    using Microsoft.Practices.Unity;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    public class ScrollTests : TestBase
    {
        #region SetUp

        protected string TestPagePath
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uriBuilder = new UriBuilder(codeBase);
                string assemblyPath = Uri.UnescapeDataString(uriBuilder.Path);
                return Path.Combine(Path.GetDirectoryName(assemblyPath), "Resources", "ScrollTest.html");
            }
        }

        protected ScrollTestPage Page
        {
            get
            {
                return this.GetContainer<ScrollTestPage>();
            }
        }

        [SetUp]
        public void BeforeTest()
        {
            Logger.Info("Start test: {0}", TestContext.CurrentContext.Test.FullName);
            this.WebDriver = new ChromeDriver();
            this.WebDriver.Navigate().GoToUrl(this.TestPagePath);
        }

        [TearDown]
        public void AfterTest()
        {
            this.QuitBrowsersFromAllSessions();

            Logger.Info("Test result: {0}", TestContext.CurrentContext.Result.Outcome.Status);
            Logger.Info("-----------");
        }

        #endregion


        [Test]
        public void ScrollToTopButtonBigMargin()
        {
            this.RegisterNewSettings(100, 100);
            this.Page.ScrollTopButton.Perform.Click();
            this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonBigMargin()
        {
            this.RegisterNewSettings(100, 100);
            this.Page.ScrollBottomButton.Perform.Click();
            this.Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        [Test]
        public void ScrollToBottomAndThenTopButtonBigMargin()
        {
            this.RegisterNewSettings(100, 100);
            this.Page.ScrollBottomButton.WebElementProvider.WebElement.ScrollToMargin(this.WebDriver, 0, 0);

            this.Page.ScrollTopButton.Perform.Click();
            this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }



        [Test]
        public void ScrollToTopButtonMinMargin()
        {
            this.RegisterNewSettings(50, 50);
            this.Page.ScrollTopButton.Perform.Click();
            this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonMinMargin()
        {
            this.RegisterNewSettings(50, 50);
            this.Page.ScrollBottomButton.Perform.Click();
            this.Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        [Test]
        public void ScrollToBottomAndThenTopButtonMinMargin()
        {
            this.RegisterNewSettings(50, 50);
            this.Page.ScrollBottomButton.WebElementProvider.WebElement.ScrollToMargin(this.WebDriver, 0, 0);

            this.Page.ScrollTopButton.Perform.Click();
            this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }



        [Test]
        public void ScrollToTopButtonNoMargin()
        {
            this.RegisterNewSettings(0, 0);
            this.Page.ScrollTopButton.Perform.Click();
            this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonNoMargin()
        {
            this.RegisterNewSettings(0, 0);
            try
            {
                this.Page.ScrollBottomButton.Perform.Click();
                this.Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
                Assert.Fail("Button shouldn't be clickable");
            }
            catch (InvalidOperationException)
            {
                // OK
            }
            catch (WebDriverException)
            {
                // OK
            }
        }

        [Test]
        public void ScrollToBottomAndThenTopButtonNoMargin()
        {
            this.RegisterNewSettings(0, 0);
            this.Page.ScrollBottomButton.WebElementProvider.WebElement.ScrollToMargin(this.WebDriver, 0, 0);

            try
            {             
                this.Page.ScrollTopButton.Perform.Click();
                this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
                Assert.Fail("Button shouldn't be clickable");
            } catch (InvalidOperationException)
            {
                // OK
            } catch (WebDriverException)
            {
                // OK
            }
        }

        [Test]
        public void ScrollToTopButtonWithintMargin()
        {
            this.RegisterNewSettings(50, 50);

            // Scroll element to be hidden by top div
            IJavaScriptExecutor js = (IJavaScriptExecutor)this.WebDriver;
            var el = this.Page.ScrollTopButton.WebElementProvider.WebElement;
            int scrollPosition = int.Parse(js.ExecuteScript("return window.pageYOffset;").ToString());
            int offsetY = scrollPosition - el.Location.Y;
            js.ExecuteScript(string.Format("window.scrollBy(0,{0})", -offsetY));

            this.Page.ScrollTopButton.Perform.Click();
            this.Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonWithingMargin()
        {
            this.RegisterNewSettings(50, 50);

            // Scroll element to be hidden by bottom div
            IJavaScriptExecutor js = (IJavaScriptExecutor)this.WebDriver;
            var el = this.Page.ScrollBottomButton.WebElementProvider.WebElement;
            int scrollPosition = int.Parse(js.ExecuteScript("return window.pageYOffset;").ToString());
            int browserInnerHeight = int.Parse(js.ExecuteScript("return window.innerHeight;").ToString());
            int offsetY = el.Location.Y + el.Size.Height - (scrollPosition + browserInnerHeight);
            js.ExecuteScript(string.Format("window.scrollBy(0,{0})", offsetY));

            this.Page.ScrollBottomButton.Perform.Click();
            this.Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        [Test]
        public void PerformScrollToBottomButtonWithingMargin()
        {
            var button = this.Page.ScrollBottomButton;
            var webElement = button.WebElementProvider.WebElement;
            webElement.ScrollToMargin(this.WebDriver, 0, 0);

            try
            {
                webElement.Click();
                Assert.Fail("Button shouldn't be clickable here.");
            } catch (Exception e)
            {
                // OK
            }

            button.Perform.Scroll(50, 50);

            Assert.DoesNotThrow(() =>
            {
                webElement.Click();
            });

            this.Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        private void RegisterNewSettings(int topScrollMargin, int bottomScrollMargin)
        {
            var settings = new Settings(4000, 400, topScrollMargin, bottomScrollMargin, 6, 500);
            this.Extensions.SetSettings(settings);
        }
    }
}
