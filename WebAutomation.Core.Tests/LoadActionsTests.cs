﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using WebAutomation.Core.Tests.Activities;
    using WebAutomation.Core.Tests.PageDefinitions;
    using NUnit.Framework;
    using System.IO;

    public class LoadActionsTests : Test
    {
        [Test]
        public void ActivityContainerTest()
        {
            string ExpectedFilename = Path.Combine(Test.AssemblyDirectory, "TestWebContainerLoadActionPage.txt");
            if (File.Exists(ExpectedFilename))
            {
                File.Delete(ExpectedFilename);
            }

            // Activity is not registerd
            this.GetContainer<TestWebContainerLoadActionPage>();
            Assert.That(File.Exists(ExpectedFilename), Is.False);

            // Register activity and load the same container again
            this.Extensions.RegisterActivity<SaveSourcesActivity>();
            this.GetContainer<TestWebContainerLoadActionPage>(true);
            Assert.That(File.Exists(ExpectedFilename), Is.True);
        }

        [Test]
        public void ActivityComponentTest()
        {
            // Value is not entered
            var page = this.GetContainer<FakePage>();
            page.TestLoadActionTextbox.Assert.Has.Value(string.Empty);

            // Register load action and load the same container again
            this.Extensions.RegisterActivity<FillValueActivity>();
            this.GetContainer<FakePage>(true);
            page.TestLoadActionTextbox.Assert.Has.Value("new value entered!");

            // Change the value and load the container again
            page.TestLoadActionTextbox.Perform.Fill("cahnged value");
            page.TestLoadActionTextbox.Assert.Has.Value("cahnged value");

            this.GetContainer<FakePage>(true);
            page.TestLoadActionTextbox.Assert.Has.Value("new value entered!");
            
        }
    }
}
