﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using Activities.OnAction;
    using Helpers;
    using NUnit.Framework;
    using WebAutomation.Core.Tests.PageDefinitions;

    [Parallelizable(ParallelScope.None)]
    public class OnActionActivityTests : Test
    {
        [SetUp]
        public void SetUp()
        {
            OnActionTestState.Logs.Clear();

            // Without Attributes
            this.Extensions.RegisterActivity<BeforeActionActivity>();
            this.Extensions.RegisterActivity<AfterActionActivity>();
            this.Extensions.RegisterActivity<BeforeAndAfterActionActivity>();
            this.Extensions.RegisterActivity<NoneActionActivity>();

            // With Attributes
            this.Extensions.RegisterActivity<AttributeBeforeActionActivity>();
            this.Extensions.RegisterActivity<AttributeAfterActionActivity>();
        }

        [Test]
        public void OnActionWithoutAttributes()
        {
            var page = this.GetContainer<SampleWebSite>();
            page.EnabledButton.Perform.Click();
            Assert.That(page.EnabledButton.Has.Value("Enabled button clicked"));

            Assert.That(OnActionTestState.Logs.Count, Is.EqualTo(4));
            Assert.That(OnActionTestState.Logs[0], Is.EqualTo("BeforeActionActivity Before "));
            Assert.That(OnActionTestState.Logs[1], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[2], Is.EqualTo("AfterActionActivity After "));
            Assert.That(OnActionTestState.Logs[3], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
        }

        [Test]
        public void OnActionOptionalWithoutAttributes()
        {
            var page = this.GetContainer<SampleWebSite>();
            page.ElementNull.PerformIfExists.Fill("test");

            Assert.That(OnActionTestState.Logs.Count, Is.EqualTo(4));
            Assert.That(OnActionTestState.Logs[0], Is.EqualTo("BeforeActionActivity Before "));
            Assert.That(OnActionTestState.Logs[1], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[2], Is.EqualTo("AfterActionActivity After "));
            Assert.That(OnActionTestState.Logs[3], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
        }

        [Test]
        public void OnActionBeforeWithAttributes()
        {
            var page = this.GetContainer<SampleWebSite>();
            page.TextSpan.Perform.Hover();

            Assert.That(OnActionTestState.Logs.Count, Is.EqualTo(5));
            Assert.That(OnActionTestState.Logs[0], Is.EqualTo("BeforeActionActivity Before "));
            Assert.That(OnActionTestState.Logs[1], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[2], Is.EqualTo("AttributeBeforeActionActivity Before 1"));
            Assert.That(OnActionTestState.Logs[3], Is.EqualTo("AfterActionActivity After "));
            Assert.That(OnActionTestState.Logs[4], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
        }

        [Test]
        public void OnActionAfterWithAttributes()
        {
            var page = this.GetContainer<SampleWebSite>();
            page.DisabledButton.Perform.Hover();

            Assert.That(OnActionTestState.Logs.Count, Is.EqualTo(5));
            Assert.That(OnActionTestState.Logs[0], Is.EqualTo("BeforeActionActivity Before "));
            Assert.That(OnActionTestState.Logs[1], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[2], Is.EqualTo("AfterActionActivity After "));
            Assert.That(OnActionTestState.Logs[3], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[4], Is.EqualTo("AttributeAfterActionActivity After "));
        }


        [Test]
        public void OnActionBeforeAndAfterWithAttributes()
        {
            var page = this.GetContainer<SampleWebSite>();
            page.DivWithBackground.Perform.Hover();

            Assert.That(OnActionTestState.Logs.Count, Is.EqualTo(6));
            Assert.That(OnActionTestState.Logs[0], Is.EqualTo("BeforeActionActivity Before "));
            Assert.That(OnActionTestState.Logs[1], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[2], Is.EqualTo("AttributeBeforeActionActivity Before 2"));
            Assert.That(OnActionTestState.Logs[3], Is.EqualTo("AfterActionActivity After "));
            Assert.That(OnActionTestState.Logs[4], Is.EqualTo("BeforeAndAfterActionActivity BeforeAndAfter "));
            Assert.That(OnActionTestState.Logs[5], Is.EqualTo("AttributeAfterActionActivity After "));
        }
    }
}
