﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using WebAutomation.Core.Tests.PageDefinitions;
    using NUnit.Framework;

    public class PropertiesTests : Test
    {
        protected SampleWebSite Page
        {
            get
            {
                return this.GetContainer<SampleWebSite>();
            }
        }

        [Test]
        public void PropertyName()
        {
            Assert.That(this.Page.DivWithBackground.Properties.ContainsKey("name"), Is.True);
            Assert.That(this.Page.DivWithBackground.Properties["name"], Is.EqualTo("DivWithBackground"));
        }

        [Test]
        public void PropertyXPath()
        {
            Assert.That(this.Page.DivWithBackground.Properties.ContainsKey("xpath"), Is.True);
            Assert.That(this.Page.DivWithBackground.Properties["xpath"], Is.EqualTo("//*[@id='DivWithBackground']"));
        }

        [Test]
        public void PropertyFullname()
        {
            Assert.That(this.Page.DivWithBackground.Properties.ContainsKey("fullname"), Is.True);
            Assert.That(this.Page.DivWithBackground.Properties["fullname"], Is.EqualTo("SampleWebSite.DivWithBackground"));
        }

        [Test]
        public void PropertyNumber()
        {
            Assert.That(this.Page.DivWithBackground.Properties.Keys.Count, Is.EqualTo(5));
        }

        [Test]
        public void PropertyWith()
        {
            var weComponent = this.Page.ParagraphNestedDefinitionByText.With("test", "test");

            Assert.That(weComponent.Properties.Keys.Count, Is.EqualTo(3));
            Assert.That(weComponent.Properties["name"], Is.EqualTo("ParagraphNestedDefinitionByText"));
            Assert.That(weComponent.Properties["fullname"], Is.EqualTo("SampleWebSite.ParagraphNestedDefinitionByText"));
            Assert.That(weComponent.Properties["pxpath"], Is.EqualTo("//p"));
        }
    }
}
