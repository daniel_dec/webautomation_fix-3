﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.SpecFlowGenericSteps.Tests
{
    using System;
    using System.IO;
    using System.Reflection;

    using NUnit.Framework;
    using TechTalk.SpecFlow;
    using WebAutomation.Core;
    using WebAutomation.SpecFlow;
    using OpenQA.Selenium.Chrome;

    [Binding]
    public class CommonSteps : SpecFlowTestBase
    {
        private string TestPagePath
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uriBuilder = new UriBuilder(codeBase);
                string assemblyPath = Uri.UnescapeDataString(uriBuilder.Path);
                return Path.Combine(Path.GetDirectoryName(assemblyPath), "Resources", "Page1.html");
            }
        }

        public CommonSteps(ScenarioContext scenarioContext)
            : base(scenarioContext)
        {
        }

        [Given(@"Web Component tests")]
        public void GivenWebComponentTests()
        {
            Assert.Throws<WebAutomationUsageException>(() => this.GetWebComponent("", ""));
            Assert.Throws<WebAutomationUsageException>(() => this.GetWebComponent("", "fakeComponent"));
            Assert.Throws<WebAutomationUsageException>(() => this.GetWebComponent("fakeContainer", ""));

            Assert.Throws<WebAutomationUsageException>(() => this.GetWebComponent("fakeContainer", "fakeComponent"));
            Assert.Throws<WebAutomationUsageException>(() => this.GetWebComponent("SampleWebSite", "fakeComponent"));
        }

        [Given(@"user navigates to test page")]
        public void GivenUserNavigatesToTestPage()
        {
            this.WebDriver.Navigate().GoToUrl(this.TestPagePath);
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            this.WebDriver = new ChromeDriver();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            this.QuitBrowser();
        }
    }
}