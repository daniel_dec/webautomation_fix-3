﻿Feature: ActionTests

Scenario: SpecFlow - action tests
	Given user navigates to test page

	When user checks 'SampleWebSite-Checkbox'
	When user checks 'SampleWebSite-ElementNull' if exists
	Then the 'SampleWebSite-Checkbox' is checked

	When user unchecks 'SampleWebSite-Checkbox'
	When user unchecks 'SampleWebSite-ElementNull' if exists
	Then the 'SampleWebSite-Checkbox' is not checked

	When user fills 'SampleWebSite-TextboxRandom' with 'Random test test'
	When user fills 'SampleWebSite-ElementNull' with 'Random test test' if exists
	Then the 'SampleWebSite-TextboxRandom' has value equal to 'Random test test'

	When user clears 'SampleWebSite-TextboxRandom'
	When user clears 'SampleWebSite-ElementNull' if exists
	Then the 'SampleWebSite-TextboxRandom' has value equal to ''

	When user hovers 'SampleWebSite-EnabledButton'
	When user hovers 'SampleWebSite-ElementNull' if exists

	When user clicks 'SampleWebSite-EnabledButton'
	When user clicks 'SampleWebSite-EnabledButton' if exists
	Then the 'SampleWebSite-EnabledButton' has value equal to 'Enabled button clicked'

	When user selects by text 'SampleWebSite-SelectRandom' with 'Option 3'
	When user selects by text 'SampleWebSite-EnabledButton' with 'Option 3' if exists
	Then the 'SampleWebSite-SelectRandom' has option 'Option 3' selected