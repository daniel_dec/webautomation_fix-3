﻿Feature: SpecFlowFeature1

Scenario: SpecFlow - get web Component
	Given Web Component tests

Scenario: : SpecFlow - steps with parameters
	Given user navigates to test page
	Then the 'SampleWebSite-TableRow' is displayed
		| Header1   | Header2   | Header3   | Header4   |
		| Row 1 C 1 | Row 1 C 2 | Row 1 C 3 | Row 1 C 4 |
	Then the 'SampleWebSite-TableRow' is displayed
		| Header1   | Header2   | Header3   | Header4   |
		| Row 1 C 1 | Row 1 C 2 | Row 1 C 3 | Row 1 C 4 |
		| Row 2 C 1 | Row 2 C 2 | Row 2 C 3 | Row 2 C 4 |
		| Row 3 C 1 | Row 3 C 2 | Row 3 C 3 | Row 3 C 4 |
		| Row 4 C 1 | Row 4 C 2 | Row 4 C 3 | Row 4 C 4 |
	Then the 'SampleWebSite-TableRow' is not displayed
		| Header1   | Header2   | Header3   | Header4   |
		| Row 1 C 1 | Row 1 C 2 | Row 1 C 3 | Row 1 C 5 |
		| Row 2 C 1 | Row 2 C 2 | Row 2 C 3 | Row 2 C 5 |